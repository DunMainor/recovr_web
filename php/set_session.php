<?php
session_start();
$reply['success'] = 'false';
if(!isset($_POST['user_name'])){
    $reply['success'] = 'false';
}elseif(!isset($_POST['user_email'])){
    $reply['success'] = 'false';
}elseif(!isset($_POST['user_token'])){
    $reply['success'] = 'false';
}elseif(!isset($_POST['user_firebase_key'])){
    $reply['success'] = 'false';
}elseif(!isset($_POST['user_phone'])){
    $reply['success'] = 'false';
}else{
    $_SESSION['user_name'] = $_POST['user_name'];
    $_SESSION['user_email'] = $_POST['user_email'];
    $_SESSION['user_token'] = $_POST['user_token'];
    $_SESSION['user_phone'] = $_POST['user_phone'];
    $_SESSION['user_firebase_key'] = $_POST['user_firebase_key'];

    $reply['success'] = 'true';
}

echo json_encode($reply)

?>
