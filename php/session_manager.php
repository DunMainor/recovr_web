<?php
    session_start();
    if(!isset($_SESSION['user_name'])){
        $reply['success'] = 'false';
    }elseif(!isset($_SESSION['user_email'])){
        $reply['success'] = 'false';
    }elseif(!isset($_SESSION['user_token'])){
        $reply['success'] = 'false';
    }elseif(!isset($_SESSION['user_firebase_key'])){
        $reply['success'] = 'false';
    }else{
        $user_name = $_SESSION['user_name'];
        $user_email = $_SESSION['user_email'];
        $user_token = $_SESSION['user_token'];
        $user_firebase_key = $_SESSION['user_firebase_key'];

    }
?>