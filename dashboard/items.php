<?php
include_once "../php/session_manager.php";
	// print_r ($_SESSION);
	if(!isset($_SESSION['user_name'])){
		header('Location: http://localhost/recovr/',true,301);
	}
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>RecovR Dashboard</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />


        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet" />

        <!--  Paper Dashboard core CSS    -->
        <link href="assets/css/paper-dashboard.css" rel="stylesheet" />


        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/main.css" rel="stylesheet" />


        <!--  Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/themify-icons.css" rel="stylesheet">

    </head>

    <body>

        <div class="wrapper">
            <div class="sidebar" data-background-color="black" data-active-color="danger">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://localhost/recovr/" class="simple-text">
                            <img src="../assets/img/logo.png" alt="logo" style="width:80%;">
                        </a>
                    </div>

                    <ul class="nav">
                        <li>
                            <a href="dashboard.php">
                                <i class="ti-panel"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="profile.php">
                                <i class="ti-user"></i>
                                <p>Profile</p>
                            </a>
                        </li>

                        <li class="active">
                            <a href="items.php">
                                <i class="ti-pencil-alt2"></i>
                                <p>Items</p>
                            </a>
                        </li>
                        <li class="active-pro">
                            <a href="http://localhost/recovr/">
                                <i class="fa fa-lg fa-arrow-left"></i>
                                <p>Back to Home</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default" style="background:rgba(10,10,10,.9);">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar1"></span>
                                <span class="icon-bar bar2"></span>
                                <span class="icon-bar bar3"></span>
                            </button>
                            <a class="navbar-brand" style="color:#daa521;" href="#">Your Stuff</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="ti-bell"></i>
                                        <p class="notification"></p>
                                        <p style="color:#daa521;">Notifications</p>
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content" style="background:url('assets/img/dash_bg.jpg'); background-size:cover; ">
                    <div class="container-fluid">
                        <div class="row">
                            <div style="max-height:500px;min-height:500px;overflow:scroll" class="col-md-4">
                                <div class="thumbnail">
                                    <img style="max-height:200px;resize-mode:contain" src="../assets/img/items/item_laptop.jpg" alt="" class="img-responsive">
                                    <div class="caption">
                                        <h4 class="pull-right text text-success ">Available</h4>
                                        <h4>
                                            <a href="#">Lenovo Laptop</a>
                                        </h4>
                                        <small class="text text-warning">Description</small>
                                        <p class="text info article">Lenovo G50-70 59-413724 is a budget laptop for everyday use. It runs on Windows 8.1
                                            64 bit OS, Intel Core i3 (4th generation) 1.7 GHz processor and has Intel HD
                                            Graphics 4400 processor. The laptop has a 15.6 inch HD LED Glare Flat Display
                                            and 1366 x 768 pixels resolution.</p>
                                    </div>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground text-center">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-shopping-cart"></i>
                                            Mark as Lost
                                        </button>
                                    </div>
                                    <div class="space-ten"></div>
                                </div>
                            </div>
                            <div style="max-height:500px;min-height:500px;overflow:scroll" class="col-md-4">
                                <div class="thumbnail">
                                    <img style="max-height:200px;resize-mode:contain;blur(5px)" src="../assets/img/items/item_title.jpg" alt="" class="img-responsive">
                                    <div class="caption">
                                        <h4 class="pull-right text text-success ">Available</h4>
                                        <h4>
                                            <a href="#">Title Deed</a>
                                        </h4>
                                        <small class="text text-warning">Description</small>
                                        <p class="text info article">A deed is any legal instrument in writing which passes, affirms or confirms an interest, right, or property and that is signed, attested, delivered, and in some jurisdictions, sealed.</p>
                                    </div>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground text-center">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-shopping-cart"></i>
                                            Mark as Lost
                                        </button>
                                    </div>
                                    <div class="space-ten"></div>
                                </div>
                            </div>
                            <div style="max-height:500px;min-height:500px;overflow:scroll" class="col-md-4">
                                <div class="thumbnail">
                                    <img style="max-height:200px;resize-mode:contain" src="../assets/img/items/item_handbag.jpg" alt="" class="img-responsive">
                                    <div class="caption">
                                        <h4 class="pull-right text text-success ">Available</h4>
                                        <h4>
                                            <a href="#">Leather Handbag</a>
                                        </h4>
                                        <small class="text text-warning">Description</small>
                                        <p class="text info article">A handbag, also called purse or pouch in North American English, is a handled medium-to-large bag used to carry personal items. Contents. [hide]. 1 "Purse" or "handbag" or "pouch"; 2 Modern origin; 3 20th century; 4 Men's bags; 5 Types.</p>
                                    </div>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground text-center">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-shopping-cart"></i>
                                            Mark as Lost
                                        </button>
                                    </div>
                                    <div class="space-ten"></div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top:30px" class="row">
                            <div style="max-height:500px;min-height:500px;overflow:scroll" class="col-md-4">
                                <div class="thumbnail">
                                    <img style="max-height:200px;resize-mode:contain" src="../assets/img/items/item_laptop.jpg" alt="" class="img-responsive">
                                    <div class="caption">
                                        <h4 class="pull-right text text-success ">Available</h4>
                                        <h4>
                                            <a href="#">National ID </a>
                                        </h4>
                                        <small class="text text-warning">Description</small>
                                        <p class="text info article">A national identification number, national identity number, or national insurance number is used by the governments of many countries as a means of tracking their citizens, permanent residents, and temporary residents for the purposes of work, taxation, government benefits and health care</p>
                                    </div>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground text-center">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-shopping-cart"></i>
                                            Mark as Lost
                                        </button>
                                    </div>
                                    <div class="space-ten"></div>
                                </div>
                            </div>
                            <div style="max-height:500px;min-height:500px;overflow:scroll" class="col-md-4">
                                <div class="thumbnail">
                                    <img style="max-height:200px;resize-mode:contain" src="../assets/img/items/item_smartphone.jpg" alt="" class="img-responsive">
                                    <div class="caption">
                                        <h4 class="pull-right text text-success ">Available</h4>
                                        <h4>
                                            <a href="#">Galaxy S9+</a>
                                        </h4>
                                        <small class="text text-warning">Description</small>
                                        <p class="text info article">Samsung's new Galaxy S9 and Galaxy S9 Plus are the two best Android phones on the market right now, hands down. We explained why in our in-depth Galaxy S9 review, but it boils down to this: Samsung's new flagship phones are the total package.</p>
                                    </div>

                                    <div class="space-ten"></div>
                                    <div class="btn-ground text-center">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-shopping-cart"></i>
                                            Mark as Lost
                                        </button>
                                    </div>
                                    <div class="space-ten"></div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>

                            <li>
                                <a href="http://localhost/recovr/">
                                    RecovR
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost/recovr/contact.php">
                                    Contact
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost/recovr/about.php">
                                    About Us
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with
                        <i class="fa fa-heart heart"></i> by
                        <a href="http://localhost/recovr/">RecovR</a>
                    </div>
                </div>
            </footer>

        </div>
        </div>

        <div class="modal product_view" id="product_view" style="z-inex:100">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="#" data-dismiss="modal" class="class pull-right">
                            <span class="fa fa-xl fa-times"></span>
                        </a>
                        <h3 class="modal-title">Your recovR item Profile</h3>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 product_img" style="display:flex; flex-direction:column">
                                <img style="flex:1;margin-botton:10px;max-height:150px;max-width:150px" src="https://narusorn18.files.wordpress.com/2017/12/qr-code.png?w=289&h=289"
                                    class="img-responsive">
                                <img style="flex:3;max-height:350px;max-width:200px" src="http://img.bbystatic.com/BestBuy_US/images/products/5613/5613060_sd.jpg"
                                    class="img-responsive">
                            </div>
                            <div class="col-md-8 product_content">
                                <h4>Item:
                                    <span>Item name</span>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                    took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply
                                    dummy text of the printing and typesetting industry.</p>
                                <h3 class="cost">
                                    <span style="margin-right:10px" class="fa fa-lg fa-qrcode"></span>
                                    66
                                    <small style="margin-left:2px" class="pre-cost">
                                        scans
                                    </small>
                                </h3>

                                <div class="space-ten"></div>
                                <div class="btn-ground row">
                                    <button type="button" class="btn btn-primary col-sm-6">
                                        <span class="glyphicon glyphicon-shopping-cart"></span> Add To Cart</button>
                                    <button type="button" class="btn btn-primary col-sm-6">
                                        <span class="glyphicon glyphicon-heart"></span> Add To Wishlist</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="assets/js/paper-dashboard.js"></script>
    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="assets/js/dash.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

        });

        function reduceModalIndex() {
            $('.modal-open').attr('z-index', '2000')
        }
    </script>

    </html>