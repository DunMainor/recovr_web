<?php
include_once "php/session_manager.php";
	// print_r ($_SESSION);
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="iglyphic">
	<link rel="shortcut icon" href="assets/img/logo.png">
	<title>recovR - Report Item</title>
	<!--Jquery-->
	<script src="vendor/jquery/jquery-3.2.1.min.js" ></script>
	<script src="vendor/hash/hash_functions.js" type="text/javascript" ></script>
	<script src="assets/js/bootbox.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js'></script>
	<!-- Bootstrap Javascript -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/smoothscroll.js"></script>
	<script src="assets/js/wow.min.js"></script>

	<!-- Bootstrap css -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"> -->
	<!-- Mynav Bar Script -->
	<!-- Custom Javascript -->
	<script src="assets/js/custom.js"></script>
	<!-- reset CSS -->
	<link href="assets/css/reset.css" rel="stylesheet">
	<!-- icon CSS -->
	<link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main css -->

	<link href="assets/css/main.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="assets/css/master.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="style-2 nav-on-header">
<!-- Start Navigation bar -->
<nav class="navbar">
	<div class="container">
		<!-- Logo -->
		<div class="pull-left">
			<a class="navbar-toggle" href="#" data-toggle="offcanvas">
				<i class="ti-menu"></i>
			</a>
			<div class="logo-wrapper">
				<a class="logo" href="index.php">
					<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
				</a>
				<a class="logo-alt" href="index.php">
					<img src="assets/img/logo.png" alt="logo-alt">
				</a>
			</div>
		</div>
		<!-- END Logo -->
		<!-- User account -->
		<div class="pull-right user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<a class="btn btn-sm btn-primary" href="Logout.php">Logout</a>';
			}else{
				echo '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
							<a href="Register.php">Register</a>';
			} ?>

		</div>
		<!-- END User account -->
		<!-- END User account -->

		<!-- Navigation menu -->
		<ul class="nav-menu user-login">
			<?php if(isset($_SESSION['user_name'])){
				echo '<li><a href="dashboard/dashboard.php">Dashboard</a></li>';
			} ?>
			<li>
				<a  href="index.php">Home</a>
			</li>
			<li>
				<a   href="Download.php">Download</a>
			</li>
			<li>
				<a class="active" href="Report.php">Report</a>
			</li>
			<li>
				<a  href="About.php">About Us</a>
			</li>
			<li>
				<a  href="Contact.php">Contact Us</a>
			</li>
		</ul>
		<!-- END Navigation menu -->
	</div>
</nav>
<!-- End Navigation bar -->
<!-- Start Pages Title  -->
<section id="page-title" class="page-title-style2">
	<div class="color-overlay"></div>
	<div class="container inner-img">
		<div class="row">
			<div class="Page-title">
				<div class="col-md-12 text-center">
					<div class="title-text">
						<h3 style="color:#fff;" class="page-title">Report Lost or Found Item</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Pages Title  -->
<script type="text/javascript">
	$(document).ready(function() {
    $('#addForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            item_key: {
                validators: {
                    notEmpty: {
                        message: "Please provide the Item Key. eg, ID No., Birth Cert No... etc"
                    }
                }
            },
             founder_name: {
                validators: {
                    notEmpty: {
                        message: "Please provide your name"
                    }
                }
            },
             phone_no: {
                validators: {
                    notEmpty: {
                        message: "Please provide your phone number"
                    }
                }
            },
            description: {
            	validators: {
            		notEmpty: {
            			message: "Please describe the found item"
            		}
            	}
            }
        }
    })
  .on('success.form.bv', function(e) {
      // Prevent form submission
      e.preventDefault();
      // Get the form instance
      var $form = $(e.target);
      // Get the BootstrapValidator instance
      var bv = $form.data('bootstrapValidator');
      // Use Ajax to submit form data
      $.ajax({
	    url: 'Report/foundItem',
	    type: 'post',
	    data: $('#addForm :input'),
	    dataType: 'html',
	    success: function(html) {
			bootbox.alert(html);
		   if(html == 'Item Recorded Successfully' || html == 'Item Recorded Successfully, we will contact you as soon as we receive it')
			{
			   $('#addForm')[0].reset();
			   location.reload();
			}
	    }
    });
  });
});
</script>
<!-- Content Start -->
<!-- report item page -->
<div class="container">
	<div class="row">
        <div class="col-lg-12 col-md-12 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a onclick="javascript:activate(this)" class="list-group-item active text-center">
                  <h4 class="fa fa-lg fa-ban"></h4><br/>Lost It
                </a>
                <a onclick="javascript:activate(this)" class="list-group-item text-center">
                  <h4 class="fa fa-lg fa-search"></h4><br/>Found It
                </a>

              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- lost section -->
                <div class="bhoechie-tab-content active">
									<div class="row">
										<div class="col-md-9">
															<div class="reservation-left-side">
																	<div class="form-horizontal">
																			<!-- Form Horizontal -->
																			<form id="addForm"  name="regForm" method="post" action="">
																			<div class="form-group">
																					<!-- Full Name -->
																					<label class="col-sm-3 control-label">Item Type&nbsp;*</label>
																					<div class="col-sm-9">
																						<select class="form-control custom-time" id="reservation_time" name="item_type">
																							<option class="options" value="OTHER">ITEM TYPE</option>
																							<option value="Passport">Passport</option>
																							<option value="National ID">National ID</option>
																							<option value="Tittle Deed">Tittle Deed</option>
																							<option value="Logbook">Logbook</option>
																							<option value="Driving License">Driving License</option>
																							<option value="ATM Card">ATM Card</option>
																							<option value="Other">Other</option>
																							<option value="Birth certificate">Birth certificate</option>
																							<option value="Death Certificate">Death Certificate</option>
																							<option value="N.H.I.F Card">N.H.I.F Card</option>
																							<option value="NSSF Cards">NSSF Cards</option>
																							<option value="Student ID">Student ID</option>
																							<option value="Academic Certificates">Academic Certificates</option>
																							<option value="Phone worth 1000 and below">Phone worth 1000 and below</option>
																						</select>
																					</div>
																			</div>

																			<div class="form-group">
																					<!-- Email Address -->
																					<label class="col-sm-3 control-label">Unique Key&nbsp;*</label>
																					<div class="col-sm-9">
																							<input type="text" name="item_key" placeholder="eg. ID No., Passport No. etc" class="form-control">
																					</div>
																			</div>
																			<!-- Email Address end -->
																			<div class="form-group">
																					<label class="col-sm-3 control-label">Your Name&nbsp;*</label>
																					<div class="col-sm-9">
																					<input type="text" name="founder_name" placeholder="Your Name" class="form-control" value="<?php if(isset($_SESSION['user_name'])){echo $_SESSION['user_name'];} ?>">
																			</div>
																			</div>
																			 <div class="form-group">
																					<label class="col-sm-3 control-label">Your Phone No.&nbsp;*</label>
																					<div class="col-sm-9">
																					<input type="number" name="phone_no" placeholder="+254700000000" class="form-control" value="<?php if(isset($_SESSION['user_phone'])){echo $_SESSION['user_phone'];} ?>">
																				</div>
																			</div>
																			<div class="form-group">
																					<!-- Special Guest -->
																					<label class="col-sm-3 control-label">Item Description</label>
																					<div class="col-sm-9">
																							<textarea placeholder="Item Description" name="description" rows="3" tabindex="4" style="resize: none;" id="special_request" class="form-control"></textarea>
																					</div>
																			</div>
																			<!-- Special Guest end -->
																			<div class="form-group">
																					<!-- Book a table -->
																					<div class="col-sm-12">
																							<!--<button class="btn btn-color" type="submit">Book a Table</button>-->
																							<button style="background-color: #EA745D; color:#FFF" class="btn btn-color pull-right" type="submit">Report Item</button>
																					</div>
																			</div>
																			<!-- Book a table end -->
																	</div>
																	<!-- Form Horizontal end -->
																	</form>
															</div>
										</div>
										<div class="col-md-3 reservation-right-side ">
											<img src="assets/img/report_lost.png" class="img-responsive" alt="report item">
										</div>
									</div>
                </div>
                <!-- found section -->
                <div class="bhoechie-tab-content">
									<div class="row">
										<div class="col-md-9">
															<div class="reservation-left-side">
																	<div class="form-horizontal">
																			<!-- Form Horizontal -->
																			<form id="addForm"  name="regForm" method="post" action="">
																			<div class="form-group">
																					<!-- Full Name -->
																					<label class="col-sm-3 control-label">Item Type&nbsp;*</label>
																					<div class="col-sm-9">
																<select class="form-control custom-time" id="reservation_time" name="item_type">
																	<option class="options" value="OTHER">ITEM TYPE</option>
																	<option value="Passport">Passport</option>
																	<option value="National ID">National ID</option>
																	<option value="Tittle Deed">Tittle Deed</option>
																	<option value="Logbook">Logbook</option>
																	<option value="Driving License">Driving License</option>
																	<option value="ATM Card">ATM Card</option>
																	<option value="Other">Other</option>
																	<option value="Birth certificate">Birth certificate</option>
																	<option value="Death Certificate">Death Certificate</option>
																	<option value="N.H.I.F Card">N.H.I.F Card</option>
																	<option value="NSSF Cards">NSSF Cards</option>
																	<option value="Student ID">Student ID</option>
																	<option value="Academic Certificates">Academic Certificates</option>
																	<option value="Phone worth 1000 and below">Phone worth 1000 and below</option>
																</select>
																					</div>
																			</div>

																			<!-- Full Name end -->
																			<div class="form-group">
																					<!-- Email Address -->
																					<label class="col-sm-3 control-label">Unique Key&nbsp;*</label>
																					<div class="col-sm-9">
																							<input type="text" name="item_key" placeholder="eg. ID No., Passport No. etc" class="form-control">
																					</div>
																			</div>
																			<!-- Email Address end -->
																			<div class="form-group">
																					<label class="col-sm-3 control-label">Your Name&nbsp;*</label>
																					<div class="col-sm-9">
																					<input type="text" name="founder_name" placeholder="Your Name" class="form-control" value="<?php if(isset($_SESSION['user_name'])){echo $_SESSION['user_name'];} ?>" >
																					</div>
																			</div>
																			 <div class="form-group">
																					<label class="col-sm-3 control-label">Your Phone No.&nbsp;*</label>
																					<div class="col-sm-9">
																					<input type="number" name="phone_no" placeholder="+254700000000" class="form-control" value="<?php if(isset($_SESSION['user_phone'])){echo $_SESSION['user_phone'];} ?>">
																				</div>
																			</div>
																			<div class="form-group">
																					<!-- Special Guest -->
																					<label class="col-sm-3 control-label">Item Description</label>
																					<div class="col-sm-9">
																							<textarea placeholder="Item Description" name="description" rows="3" tabindex="4" style="resize: none;" id="special_request" class="form-control"></textarea>
																					</div>
																			</div>
																			<!-- Special Guest end -->
																			<div class="form-group">
																					<!-- Book a table -->
																					<div class="col-sm-12">
																							<!--<button class="btn btn-color" type="submit">Book a Table</button>-->
																							<button style="background-color: #EA745D; color:#FFF" class="btn btn-color pull-right" type="submit">Report Item</button>
																					</div>
																			</div>
																			<!-- Book a table end -->
																	</div>
																	<!-- Form Horizontal end -->
																	</form>
															</div>
										</div>
										<div class="col-md-3 reservation-right-side ">
											<img src="assets/img/report_lost.png" class="img-responsive" alt="report item">
										</div>
									</div>
                </div>

            </div>
        </div>
  </div>
</div>
	<div class="container">
    <section>

			<!-- Main row end -->
    </section>
	</div>
	<!-- END report -->
<!-- Content End -->
<!-- start footer area -->
<section id="index-footer" class="footer-area-content">
	<!-- Newsletter -->
	<div id="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3><i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="col-sm-8">
						<input type="email" required="required" placeholder="Your Email Address" id="email" class="form-control" name="email">
					</div>
					<div class="col-sm-4">
						<a href="#" class="btn btn-subscribe">Subscribe</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END: Newsletter -->
	<div class="footer-bottom footer-wrapper style-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-bottom-navigation">
						<div class="cell-view">
							<div class="footer-links">
								<a href="#">Site Map</a>
								<a href="#">Search</a>
								<a href="#">Terms</a>
								<a href="#">Privacy policy</a>
								<a href="#">Contact Us</a>
								<a href="#">Careers</a>
							</div>
							<div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
						</div>
						<div class="cell-view">
							<div class="social-content">
								<a class="post-facebook" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
								<a class="post-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
								<a class="post-twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
								<a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- footer area end -->
<!-- Back to top Link -->
<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
</body>


<script type="text/javascript">
$(document).ready(function() {

})

 function activate(elem) {
	 $(elem).siblings('a.active').removeClass("active");
	 $(elem).addClass("active");
	 var index = $(elem).index();
	 $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
	 $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
 }
</script>
</html>
<!-- Localized -->
