<?php
include_once "php/session_manager.php";
    // print_r ($_SESSION);
?>
  <!DOCTYPE HTML>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="iglyphic">
    <link rel="shortcut icon" href="assets/img/logo.png">
    <title>recovR - Contact Us</title>
    <!-- reset CSS -->
    <link href="assets/css/reset.css" rel="stylesheet">
    <!-- Bootstrap css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- icon CSS -->
    <link href="assets/css/fontello.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Main css -->
    <link href="assets/css/main.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/results.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
  </head>

  <body class="style-2 nav-on-header">
    <!-- Start Navigation bar -->
    <nav class="navbar">
      <div class="container">
        <!-- Logo -->
        <div class="pull-left">
          <a class="navbar-toggle" href="#" data-toggle="offcanvas">
				<i class="ti-menu"></i>
			</a>
          <div class="logo-wrapper">
            <a class="logo" href="index.php">
					<img src="assets/img/logo.png" width="63px" height="60px" alt="logo">
				</a>
            <a class="logo-alt" href="index.php">
					<img src="assets/img/logo.png" alt="logo-alt">
				</a>
          </div>
        </div>
        <!-- END Logo -->
        <!-- User account -->
        <div class="pull-right user-login">
<?php if (isset($_SESSION['user_name'])) {
    echo '<a class="btn btn-sm btn-primary" href="Logout.php">Logout</a>';
} else {
    echo '<a class="btn btn-sm btn-primary" href="Login.php">Login</a> or &nbsp;
							<a href="Register.php">Register</a>';
} ?>

        </div>
        <!-- END User account -->
        <!-- END User account -->

        <!-- Navigation menu -->
        <ul class="nav-menu user-login">
          <?php if (isset($_SESSION['user_name'])) {
    echo '<li><a href="dashboard/dashboard.php">Dashboard</a></li>';
} ?>
          <li>
            <a href="index.php">Home</a>
          </li>
          <li>
            <a href="Download.php">Download</a>
          </li>
          <li>
            <a href="Report.php">Report</a>
          </li>
          <li>
            <a href="About.php">About Us</a>
          </li>
          <li>
            <a class="active" href="Contact.php">Contact Us</a>
          </li>
        </ul>
        <!-- END Navigation menu -->
      </div>
    </nav>
    <!-- End Navigation bar -->
    <!-- Start Pages Title  -->
    <section id="page-title" class="page-title-style2">
      <div class="color-overlay"></div>
      <div class="container inner-img">
        <div class="row">
          <div class="Page-title">
            <div class="col-md-12 text-center">
              <div class="title-text">
                <h3 style="color:#fff;" class="page-title">Search Results</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Pages Title  -->
    <!-- Content Start -->
    <main class="main-content contact-page-two">
      <!-- <div class="result-card container product_view" id="product_view" style="z-inex:100">
        <div class=" result-card-inner">
          <div class=" result-card-further container-fluid">
            <div class="header" style="background:rgba(255,255,255,.8);align-items:center;justify-content:center" >

            </div>
            <div class="modal-body" style="background:rgba(255,255,255,.5);border-radius:20px;" >
              <div class="row" style="height:100%" >
                <div class="col-md-4 product_img" style="display:flex; flex-direction:column;background:rgba(255,255,255,.3);padding:10px;">
                  <img style="flex:1;margin:auto;max-height:150px;max-width:150px;position:absolute;top:-45px;left:-65px;" src="https://narusorn18.files.wordpress.com/2017/12/qr-code.png?w=289&h=289" class="img-responsive">
                  <div class="space-ten"></div>
                  <img style="flex:3;max-height:500px;max-width:250px" src="http://img.bbystatic.com/BestBuy_US/images/products/5613/5613060_sd.jpg" class="img-responsive">
                </div>
                <div class="col-md-8 product_content" style="background:#f8f8f8;height:100%" >
                  <h4>Item: <span>Item name</span></h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
                    book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                  <h3 class="cost">
                          <span style="margin-right:10px" class="fa fa-lg fa-qrcode"></span>
                             66
                           <small style="margin-left:2px" class="pre-cost">
                             scans
                          </small>
                        </h3>

                  <div class="space-ten"></div>
                  <div class="btn-ground row">
                    <button type="button" style="margin-left:50px;margin-right: 50px;" class="btn btn-primary col-sm-6 col-lg-4 col-md-4"><span class="glyphicon glyphicon-shopping-cart"></span> Claim</button>
                    <button type="button" style="margin-left:50px;margin-right: 50px;" class="btn btn-primary col-sm-6 col-lg-4 col-md-4"><span class="glyphicon glyphicon-heart"></span> Report</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="container content product_view " style="margin:auto;width:1000px;height:500px; background:rgba(255,255,255,.5); padding:30px;"  >
          <img src="assets/img/no_results.png" alt="no results" style="width:400px;height:400px;" >
          <img src="assets/img/sad_face.png" alt="no results" style="margin-left:150;width:300px;height:400px;" >
      </div>

    </main>
    <!-- Content End -->
    <!-- start footer area -->
    <section id="index-footer" class="footer-area-content">
      <!-- Newsletter -->
      <div id="newsletter">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <h3><i class="fa fa-envelope-o"></i>Keep in touch, Join our newsletter</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="col-sm-8">
                <input type="email" required="required" placeholder="Your Email Address" id="email" class="form-control" name="email">
              </div>
              <div class="col-sm-4">
                <a href="#" class="btn btn-subscribe">Subscribe</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END: Newsletter -->
      <div class="footer-bottom footer-wrapper style-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="footer-bottom-navigation">
                <div class="cell-view">
                  <div class="footer-links">
                    <a href="#">Site Map</a>
                    <a href="#">Search</a>
                    <a href="#">Terms</a>
                    <a href="#">Privacy policy</a>
                    <a href="#">Contact Us</a>
                    <a href="#">Careers</a>
                  </div>
                  <div class="copyright">All right reserved. &copy; <a target="_blank" href="index.php">recovR</a><small>   Your Lost Identifiable Items Recovery Experts</small></div>
                </div>
                <div class="cell-view">
                  <div class="social-content">
                    <a class="post-facebook" href="https://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="post-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                    <a class="post-twitter" href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- footer area end -->
    <!-- Back to top Link -->
    <div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
    <!--Jquery-->
    <!--Jquery-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="vendor/hash/hash_functions.js" type="text/javascript"></script>
    <script src="assets/js/bootbox.min.js"></script>
    <!-- Bootstrap Javascript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/smoothscroll.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <!-- Mynav Bar Script -->
    <script src="assets/js/app.min.js"></script>
    <!-- Custom Javascript -->
    <script src="assets/js/custom.js"></script>
  </body>

  </html>
  <!-- Localized -->
